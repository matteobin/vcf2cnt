BIN     = vcf2cnt
VERSION = 1.0

PREFIX = /usr/local

INCS = 
LIBS = 

CPPFLAGS_VCF2CNT = -DVERSION=\"${VERSION}\" ${CPPFLAGS}
# debug
#CFLAGS_VCF2CNT = -ggdb -ansi -pedantic ${INCS} ${CPPFLAGS_VCF2CNT} ${CLAGS}
CFLAGS_VCF2CNT  = -Os -ansi -pedantic ${INCS} ${CPPFLAGS_VCF2CNT} ${CFLAGS}
LDFLAGS_VCF2CNT = ${LIBS} ${LDFLAGS}

SRC = vcf2cnt.c
OBJ = ${SRC:.c=.o}

all: options vcf2cnt

options: 
	@echo vcf2cnt build options:
	@echo "CFLAGS   = ${CFLAGS_VCF2CNT}"
	@echo "LDFLAGS  = ${LDFLAGS_VCF2CNT}"
	@echo "CC       = ${CC}"

.c.o:
	${CC} ${CFLAGS_VCF2CNT} -c $< -o $@
	
${BIN}: ${OBJ}
	${CC} -o ${BIN} ${OBJ} ${LDFLAGS_VCF2CNT}

clean:
	rm -f ${BIN} ${BIN}-${VERSION}.tar.gz *.o

dist: clean
	mkdir ${BIN}-${VERSION}
	cp -R ${SRC} LICENSE Makefile README ${BIN}-${VERSION}
	tar -cf ${BIN}-${VERSION}.tar ${BIN}-${VERSION}
	gzip ${BIN}-${VERSION}.tar
	rm -fr ${BIN}-${VERSION}

install: ${BIN}
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f ${BIN} ${DESTDIR}${PREFIX}/bin
	chmod 755 ${DESTDIR}${PREFIX}/bin/${BIN}

uninstall:
	rm -f ${DESTDIR}${PREFIX}/bin/${BIN}

.PHONY: all options clean dist install uninstall
